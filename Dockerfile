FROM python:3

WORKDIR /home

# Install necessary libraries to access USB VISA device
RUN apt update && apt-get install --no-install-recommends -y \
    udev \
    libusb-1.0-0

# Install Python libraries
COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

# Add the power supply test
COPY power_supply.py .