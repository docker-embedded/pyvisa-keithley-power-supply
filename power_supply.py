import pyvisa
import usbtmc
import time
import re

ps_vid = 0x05E6
ps_pid = 0x2220

def get_ps_resource(vid, pid):
    expression = 'USB[0-9]*::' + str(vid) + '::' + str(pid) + '::' + '.*'
    for resource in pyvisa.ResourceManager().list_resources():
        if (re.match(expression, resource)):
            return resource

if __name__ == "__main__":
    print("Attempting to attach to power supply VISA resource.")
    resource = get_ps_resource(ps_vid, ps_pid)
    print("Got the following resource:", resource)
    instr = pyvisa.ResourceManager().open_resource(resource)

    print("Querrying instrument with *IDN command.")
    print("Response:", instr.query("*IDN?").strip())

    print("Toggling power on and off.")
    instr.write("SYSTEM:REMOTE")
    time.sleep(0.5)
    instr.write("APPLY CH1,28V,1A")
    time.sleep(0.5)
    instr.write("CHANNEL:OUTPUT ON")
    time.sleep(1)
    instr.write("CHANNEL:OUTPUT OFF")
    instr.write("SYSTEM:LOCAL")