# PyVISA Keithley Power Supply

Docker image containing PyVISA libraries to talk to Keithley 2200 Series power supplies.

## Running a power cycle test
Plug the device into a USB port on your machine and run the follow command: 
```
$ docker run --privileged -it registry.gitlab.com/docker-embedded/pyvisa-keithley-power-supply python3 ./power_supply.py
```